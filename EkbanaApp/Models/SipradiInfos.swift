import Foundation

struct Item: Codable {
    let id: Int
    let imageUrl: String
    let link: String?
    let title: String?
    let subTitle: String?
}

struct SipradiInfo: Codable {
    let id: Int
    let type: String
    let items: [Item]?
}

class SipradiInfos {
    // MARK: public properties
    static let main = SipradiInfos()
    var sipradiInfos = [SipradiInfo]()
    
    // MARK: public methods
    public func getJsonData(completion: @escaping ([SipradiInfo]?) -> Void) {
        Network.main.getData { success, statusCode, data in
            if let _ = statusCode {
                if success, let data = data {
                    let jsonDatas = try? JSONDecoder().decode([SipradiInfo].self, from: data)
                    self.sipradiInfos.removeAll()
                    jsonDatas?.forEach({ (jsonData) in
                        self.sipradiInfos.append(jsonData)
                    })
                    completion(self.sipradiInfos)
                    return
                }
            }
            print("Failed to parse JSON data")
            completion(nil)
        }
    }
    
}
