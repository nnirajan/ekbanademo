import UIKit

class KnowMoreTableViewCell: UITableViewCell {
    
    // MARK: UI properties
    let photoView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleToFill
        return iv
    }()
    
    let subTitleLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .gray
        lbl.font = UIFont.systemFont(ofSize: textSize1)
        lbl.text = "Rewrfasfsfskjh"
        return lbl
    }()
    
    // MARK: overriden methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        setupViews()
    }
    
    // MARK: functions
    func setupViews() {
        setupPhotoView()
        setupSubTitlLabel()
    }
    
    func setupPhotoView() {
        addSubview(photoView)
        photoView.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        photoView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        photoView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        photoView.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
    func setupSubTitlLabel() {
        addSubview(subTitleLbl)
        subTitleLbl.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        subTitleLbl.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
        subTitleLbl.topAnchor.constraint(equalTo: photoView.bottomAnchor, constant: 5).isActive = true
        subTitleLbl.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
    }
    
}
