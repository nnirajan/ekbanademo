import UIKit

class LubricantsTableViewCell: UITableViewCell {

    // MARK: variables
    var items = [Item]()
    
    // MARK: UI properties
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.showsVerticalScrollIndicator = false
        cv.showsHorizontalScrollIndicator = false
        cv.backgroundColor = .clear
        return cv
    }()
    
    // MARK: overriden methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        setupViews()
    }
    
    // MARK: functions
    func setupViews() {
        setupCollectionView()
    }
    
    func setupCollectionView() {
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15).isActive = true
        collectionView.dataSource = self
        collectionView.delegate = self
    }

}

// MARK: Collectionview delegates, datasource, delegate flowlayout
extension LubricantsTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (items.count + 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row < items.count {
            collectionView.register(LubricantsCollectionViewCell.self, forCellWithReuseIdentifier: lubricantsCollectionViewCellId)
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: lubricantsCollectionViewCellId, for: indexPath) as! LubricantsCollectionViewCell
            
            let imageUrl = items[indexPath.row].imageUrl
            Network.main.getImage(imageUrl: imageUrl) { success, statusCode, data in
                if success, let data = data {
                    DispatchQueue.main.async {
                        cell.imageView.image = UIImage(data: data)
                        cell.titleLbl.text = self.items[indexPath.row].title
                    }
                }
            }
            return cell
        }
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: latestCollectionViewCellId)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: latestCollectionViewCellId, for: indexPath)
        cell.backgroundColor = .gray
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: cell.bounds.size.width, height: 200))
        lbl.center = cell.contentView.center
        lbl.textAlignment = .center
        lbl.text = "TAP TO SEE MORE"
        lbl.font = UIFont.systemFont(ofSize: textSize2)
        lbl.numberOfLines = 0
        lbl.textColor = .white
        cell.contentView.addSubview(lbl)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row < items.count {
            let height: CGFloat = collectionView.frame.height
            return CGSize(width: height, height: height)
        }
        return CGSize(width: 80, height: collectionView.frame.height)
    }
    
}

