import UIKit
import MapKit
import CoreLocation

class NearByTableViewCell: UITableViewCell {

    // MARK: variables
    private let locationManager = CLLocationManager()
    private let regionInMeters: Double = 2000
    
    // MARK: UI properties
    private var mapView: MKMapView = {
        let mp = MKMapView()
        mp.translatesAutoresizingMaskIntoConstraints = false
        return mp
    }()
    
    // MARK: overriden methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        setupMapView()
    }
    
    // MARK: functions
    func setupMapView() {
        locationManager.delegate = (self as CLLocationManagerDelegate)
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        addSubview(mapView)
        mapView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        mapView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        mapView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        mapView.delegate = self
        
        mapView.showsUserLocation = true
        
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(viewRegion, animated: false)
        }
    }

}

extension NearByTableViewCell: MKMapViewDelegate {
    // MARK: Map view delegate
}

extension NearByTableViewCell: CLLocationManagerDelegate {
    // MARK: Location manager delegate
}
