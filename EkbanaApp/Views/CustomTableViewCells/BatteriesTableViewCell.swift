import UIKit

class BatteriesTableViewCell: UITableViewCell {
    
    // MARK: variables
    var items = [Item]()
    
    // MARK: UI properties
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 10
        
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.showsVerticalScrollIndicator = false
        cv.showsHorizontalScrollIndicator = false
        cv.backgroundColor = .clear
        return cv
    }()
    
    // MARK: overriden methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        setupViews()
    }
    
    // MARK: functions
    func setupViews() {
        setupCollectionView()
    }
    
    func setupCollectionView() {
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15).isActive = true
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(BatteriesCollectionViewCell.self, forCellWithReuseIdentifier: batteriesCollectionViewCellId)
    }
    
}

// MARK: Collectionview delegates, datasource, delegate flowlayout
extension BatteriesTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: batteriesCollectionViewCellId, for: indexPath) as! BatteriesCollectionViewCell
        
        let imageUrl = items[indexPath.row].imageUrl
        Network.main.getImage(imageUrl: imageUrl) { success, statusCode, data in
            if success, let data = data {
                DispatchQueue.main.async {
                    cell.imageView.image = UIImage(data: data)
                    cell.titleLbl.text = self.items[indexPath.row].title
                }
            }
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = (collectionView.frame.width) / 2
        let height: CGFloat = (frame.height - 40) / 2
        return CGSize(width: width, height: height)
    }
    
}
