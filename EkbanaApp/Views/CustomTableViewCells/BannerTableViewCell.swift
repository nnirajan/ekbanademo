import UIKit

class BannerTableViewCell: UITableViewCell {
    
    // MARK: variables
    var bannerInfo: SipradiInfo? = nil
    var items = [Item]()
    private var timer = Timer()
    private var counter = 0
    
    // MARK: UI properties
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.showsVerticalScrollIndicator = false
        cv.showsHorizontalScrollIndicator = false
        cv.isPagingEnabled = true
        return cv
    }()
    
    let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.translatesAutoresizingMaskIntoConstraints = false
        pc.pageIndicatorTintColor = .white
        pc.currentPageIndicatorTintColor = .red
        return pc
    }()
    
    // MARK: overriden methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        setupViews()
    }
    
    // MARK: functions
    func setupViews() {      
        setupCollectionView()
        setupPageControl()
    }
    
    func setupCollectionView() {
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        collectionView.register(BannerCollectionViewCell.self, forCellWithReuseIdentifier: bannerCollectionViewCellId)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func setupPageControl() {
        addSubview(pageControl)
        pageControl.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: -20).isActive = true
        pageControl.widthAnchor.constraint(equalToConstant: 100).isActive = true
        pageControl.heightAnchor.constraint(equalToConstant: 25).isActive = true
        pageControl.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor).isActive = true
        
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
    }
    
    // MARK: Actions
    @objc private func changeImage() {
        if counter < items.count {
            let index = IndexPath.init(row: counter, section: 0)
            collectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageControl.currentPage = counter
            counter += 1
        } else {
            counter = 0
            let index = IndexPath.init(row: counter, section: 0)
            collectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            pageControl.currentPage = counter
            counter = 1
        }
    }
    
}

// MARK: Collectionview delegates, datasource, delegate flowlayout
extension BannerTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = items.count
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: bannerCollectionViewCellId, for: indexPath) as! BannerCollectionViewCell
        
        let imageUrl = items[indexPath.row].imageUrl
        Network.main.getImage(imageUrl: imageUrl) { success, statusCode, data in
            if success, let data = data {
                DispatchQueue.main.async {
                    cell.imageView.image = UIImage(data: data)
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = frame.width
        let height = frame.height
        return CGSize(width: width, height: height)
    }
    
}
