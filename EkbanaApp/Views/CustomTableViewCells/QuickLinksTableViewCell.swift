import UIKit

class QuickLinksTableViewCell: UITableViewCell {
    
    // MARK: variables
    var items = [Item]()
    private var timer = Timer()
    private var counter = 0
    
    // MARK: UI properties
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 5
        
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.showsVerticalScrollIndicator = false
        cv.showsHorizontalScrollIndicator = false
        cv.backgroundColor = .clear
        return cv
    }()
    
    let horizontalStackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.spacing = 10
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        return sv
    }()
    
    let verticalStackView: UIStackView =  {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.spacing = 10
        sv.axis = .vertical
        sv.distribution = .fillEqually
        return sv
    }()
    
    let myVehicleView = CustomButtonView(titleStr: "My Vehicle", subTitleStr: "View Vehicle Profile", imageStr: "myVehicle", backgroundColor: .cyan)
    
    let myExpensesView = CustomButtonView(titleStr: "My Expenses", subTitleStr: "All Expenses Details", imageStr: "myExpenses", backgroundColor: .blue)
    
    let registerMyBatteryView = CustomButtonView(titleStr: "Register My Battery", subTitleStr: "Register Your Multiple Exide Batteries", imageStr: "registerMyBattery", backgroundColor: .lightGray)
    
    // MARK: overriden methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        setupViews()
    }
    
    // MARK: functions
    func setupViews() {
        setupCollectionView()
        setupStackView()
    }
    
    func setupCollectionView() {
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
        collectionView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        collectionView.register(QuickLinksCollectionViewCell.self, forCellWithReuseIdentifier: quickLinksCollectionViewCellId)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func setupStackView() {
        addSubview(verticalStackView)
        verticalStackView.leadingAnchor.constraint(equalTo: collectionView.leadingAnchor).isActive = true
        verticalStackView.trailingAnchor.constraint(equalTo: collectionView.trailingAnchor).isActive = true
        verticalStackView.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: 8).isActive = true
        verticalStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
        
        horizontalStackView.addArrangedSubview(myVehicleView)
        horizontalStackView.addArrangedSubview(myExpensesView)
        
        verticalStackView.addArrangedSubview(horizontalStackView)
        verticalStackView.addArrangedSubview(registerMyBatteryView)
    }
    
}

// MARK: Collectionview delegates, datasource, delegate flowlayout
extension QuickLinksTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: quickLinksCollectionViewCellId, for: indexPath) as! QuickLinksCollectionViewCell
        
        let imageUrl = items[indexPath.row].imageUrl
        Network.main.getImage(imageUrl: imageUrl) { success, statusCode, data in
            if success, let data = data {
                DispatchQueue.main.async {
                    cell.imageView.image = UIImage(data: data)
                    cell.titleLbl.text = self.items[indexPath.row].title
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let count = CGFloat(items.count)
        let width: CGFloat = collectionView.frame.width / count
        
        return CGSize(width: width, height: width)
    }
    
}
