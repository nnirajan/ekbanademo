import UIKit

class CustomButtonView: UIView {
    
    // MARK: UI properties
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let stackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.spacing = 0
        sv.axis = .vertical
        sv.distribution = .fillEqually
        return sv
    }()
    
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: textSize1)
        lbl.textColor = .white
        return lbl
    }()
    
    let subTitleLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: textSize4)
        lbl.textColor = .white
        return lbl
    }()
    
    // MARK: Custom Init
    required init(titleStr: String, subTitleStr: String, imageStr: String, backgroundColor: UIColor) {
        super.init(frame: .zero)
        
        layer.cornerRadius = 10
        
        titleLbl.text = titleStr
        subTitleLbl.text = subTitleStr
        let image = UIImage(named: imageStr)
        imageView.image = image
        self.backgroundColor = backgroundColor
        
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: functions
    func setUpViews() {
        setupImageView()
        setupStackView()
    }
    
    func setupImageView() {
        addSubview(imageView)
        imageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        
        imageView.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        imageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
        
        imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor).isActive = true
    }
    
    func setupStackView() {
        addSubview(stackView)
        stackView.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 5).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5).isActive = true
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
        
        stackView.addArrangedSubview(titleLbl)
        stackView.addArrangedSubview(subTitleLbl)
    }
    
}
