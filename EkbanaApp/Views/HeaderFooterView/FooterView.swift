import UIKit

class FooterView: UITableViewHeaderFooterView {

    // MARK: UI properties
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .gray
        lbl.font = UIFont.systemFont(ofSize: textSize2)
        return lbl
    }()
    
    // MARK: Overridden methods
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        tintColor = .white
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Public methods
    func setupViews() {
        addSubview(titleLbl)
        titleLbl.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        titleLbl.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
}
