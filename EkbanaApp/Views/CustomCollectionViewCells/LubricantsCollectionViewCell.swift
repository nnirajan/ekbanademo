import UIKit

class LubricantsCollectionViewCell: UICollectionViewCell {
    
    // MARK: UI properties
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleToFill
        let image = UIImage(named: "loading")
        iv.image = image
        return iv
    }()
    
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: textSize3)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.textAlignment = .center
        return lbl
    }()
    
    // MARK: overrriden methods
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .clear
        layer.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 1
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: functions
    func setupViews() {
        setupImageView()
        setupTitleLabel()
    }
    
    func setupImageView() {
        addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
    }
    
    func setupTitleLabel() {
        addSubview(titleLbl)
        titleLbl.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 5).isActive = true
        titleLbl.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15).isActive = true
        titleLbl.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5).isActive = true
        titleLbl.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5).isActive = true
        titleLbl.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
}
