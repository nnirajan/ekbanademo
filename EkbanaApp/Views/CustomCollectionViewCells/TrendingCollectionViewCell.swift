import UIKit

class TrendingCollectionViewCell: UICollectionViewCell {
    
    // MARK: UI properties
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleToFill
        let image = UIImage(named: "loading")
        iv.image = image
        return iv
    }()
    
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: textSize2)
        return lbl
    }()
    
    let viewLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .gray
        lbl.font = UIFont.systemFont(ofSize: textSize4                                      )
        lbl.text = "VIEW DETAILS"
        return lbl
    }()
    
    // MARK: overrriden methods
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .clear
        layer.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 1
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: functions
    func setupViews() {
        setupImageView()
        setupTitleLabel()
        setupViewLabel()
    }
    
    func setupImageView() {
        addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
    
    func setupTitleLabel() {
        addSubview(titleLbl)
        titleLbl.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 2).isActive = true
        titleLbl.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        titleLbl.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    func setupViewLabel() {
        addSubview(viewLbl)
        viewLbl.topAnchor.constraint(equalTo: titleLbl.bottomAnchor, constant: 2).isActive = true
        viewLbl.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
        viewLbl.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        viewLbl.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
}
