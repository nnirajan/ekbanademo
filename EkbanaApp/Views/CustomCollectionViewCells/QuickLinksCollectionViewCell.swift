import UIKit

class QuickLinksCollectionViewCell: UICollectionViewCell {
    
    // MARK: UI properties
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleToFill
        let image = UIImage(named: "loading")
        iv.image = image
        return iv
    }()
    
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .gray
        lbl.font = UIFont.systemFont(ofSize: textSize5)
        return lbl
    }()
    
    // MARK: overrriden methods
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: functions
    func setupViews() {
        setupImageView()
        setupTitleLabel()
    }
    
    func setupImageView() {
        addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        let width: CGFloat = (UIScreen.main.bounds.width / 5) * 0.5
        imageView.widthAnchor.constraint(equalToConstant: width).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: width).isActive = true
    }
    
    func setupTitleLabel() {
        addSubview(titleLbl)
        titleLbl.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 2).isActive = true
        titleLbl.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
        titleLbl.centerXAnchor.constraint(equalTo: imageView.centerXAnchor).isActive = true
    }
    
}
