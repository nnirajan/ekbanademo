import UIKit

class MainVC: UITableViewController {

    // variables
    private var sipradiInfos = SipradiInfos.main.sipradiInfos
    
    private var headerSubTitles = [3: "New in the Market!", 4: "Check what others are looking for!", 5: "POWER UP YOUR LIFESTYLE", 6: "MAK YOUR ENGINE HAPPY!", 7: "Visit the nearest SIPRADI Services Around You"]
    
    private var footerTitle = [2: "KNOW MORE", 5: "TAP FOR MORE POWER"]
    
    // MARK: overridden methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Welcome to SIPRADI!"
        tableView.showsVerticalScrollIndicator = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if sipradiInfos.count == 0 {
            getSipradiInfos()
        }
    }
    
    // MARK:  Private functions
    // network call
    private func getSipradiInfos() {
        SipradiInfos.main.getJsonData { sIs in
            if let sIs = sIs {
                self.sipradiInfos = sIs
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    // MARK: - Table view data source, delegate
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sipradiInfos.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if sipradiInfos[indexPath.section].type == "BANNER" {
            if let bannerInfo = sipradiInfos.first(where: { $0.type == "BANNER"}), let items = bannerInfo.items {
                tableView.register(BannerTableViewCell.self, forCellReuseIdentifier: bannerTableViewCellId)
                
                let cell = tableView.dequeueReusableCell(withIdentifier: bannerTableViewCellId) as! BannerTableViewCell
                cell.bannerInfo = bannerInfo
                cell.items = items
                return cell
            }
        } else if sipradiInfos[indexPath.section].type == "QUICKLINKS" {
            if let quickLinksInfo = sipradiInfos.first(where: { $0.type == "QUICKLINKS"}), let items = quickLinksInfo.items {
                tableView.register(QuickLinksTableViewCell.self, forCellReuseIdentifier: quickLinksTableViewCellId)
                
                let cell = tableView.dequeueReusableCell(withIdentifier: quickLinksTableViewCellId) as! QuickLinksTableViewCell
                cell.items = items
                return cell
            }
        } else if sipradiInfos[indexPath.section].type == "KNOW MORE" {
            if let knowInfo = sipradiInfos.first(where: { $0.type == "KNOW MORE"}), let items = knowInfo.items {
                tableView.register(KnowMoreTableViewCell.self, forCellReuseIdentifier: bannerTableViewCellId)
                
                let cell = tableView.dequeueReusableCell(withIdentifier: bannerTableViewCellId) as! KnowMoreTableViewCell
                
                let imageUrl = items[0].imageUrl
                Network.main.getImage(imageUrl: imageUrl) { success, statusCode, data in
                    if success, let data = data {
                        DispatchQueue.main.async {
                            cell.subTitleLbl.text = items[0].subTitle
                            cell.photoView.image = UIImage(data: data)
                        }
                    }
                }
                return cell
            }
        } else if sipradiInfos[indexPath.section].type == "LATEST" {
            if let latestInfo = sipradiInfos.first(where: { $0.type == "LATEST"}), let items = latestInfo.items {
                tableView.register(LatestTableViewCell.self, forCellReuseIdentifier: latestTableViewCellId)
                
                let cell = tableView.dequeueReusableCell(withIdentifier: latestTableViewCellId) as! LatestTableViewCell
                cell.items = items
                return cell
            }
        }  else if sipradiInfos[indexPath.section].type == "BATTERIES" {
            if let batteriesInfo = sipradiInfos.first(where: { $0.type == "BATTERIES"}), let items = batteriesInfo.items {
                tableView.register(BatteriesTableViewCell.self, forCellReuseIdentifier: batteriesTableViewCellId)
                
                let cell = tableView.dequeueReusableCell(withIdentifier: batteriesTableViewCellId) as! BatteriesTableViewCell
                cell.items = items
                return cell
            }
        } else if sipradiInfos[indexPath.section].type == "TRENDING" {
            if let trendingInfo = sipradiInfos.first(where: { $0.type == "TRENDING"}), let items = trendingInfo.items {
                tableView.register(TrendingTableViewCell.self, forCellReuseIdentifier: trendingTableViewCellId)
                
                let cell = tableView.dequeueReusableCell(withIdentifier: trendingTableViewCellId) as! TrendingTableViewCell
                cell.items = items
                return cell
            }
        } else if sipradiInfos[indexPath.section].type == "LUBRICANTS" {
            if let lubricantsInfo = sipradiInfos.first(where: { $0.type == "LUBRICANTS"}), let items = lubricantsInfo.items {
                tableView.register(LubricantsTableViewCell.self, forCellReuseIdentifier: lubricantsTableViewCellId)
                
                let cell = tableView.dequeueReusableCell(withIdentifier: lubricantsTableViewCellId) as! LubricantsTableViewCell
                cell.items = items
                return cell
            }
        } else if sipradiInfos[indexPath.section].type == "NEARBY" {
            tableView.register(NearByTableViewCell.self, forCellReuseIdentifier: nearByTableViewCellId)
            let cell = tableView.dequeueReusableCell(withIdentifier: nearByTableViewCellId) as! NearByTableViewCell
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if sipradiInfos[section].type == "BANNER" || sipradiInfos[section].type == "KNOW MORE" {
            return 0
        }
        return 50
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if sipradiInfos[indexPath.section].type == "BANNER" {
            return bannerViewHeight
        } else if sipradiInfos[indexPath.section].type == "QUICKLINKS" {
            return quickLinksViewHeight
        } else if sipradiInfos[indexPath.section].type == "KNOW MORE" {
            return knowMoreViewHeight
        } else if sipradiInfos[indexPath.section].type == "LATEST" {
            return latestViewHeight
        } else if sipradiInfos[indexPath.section].type == "TRENDING" {
            return trendingViewHeight
        } else if sipradiInfos[indexPath.section].type == "BATTERIES" {
            return batteriesViewHeight
        } else if sipradiInfos[indexPath.section].type == "LUBRICANTS" {
            return lubricantsViewHeight
        } else if sipradiInfos[indexPath.section].type == "NEARBY" {
            return nearByViewHeight
        }
        return 50
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if sipradiInfos[section].type == "KNOW MORE" || sipradiInfos[section].type == "BATTERIES" {
            return 50
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        tableView.register(HeaderView.self, forHeaderFooterViewReuseIdentifier: headerViewId)
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerViewId) as! HeaderView
        headerView.titleLbl.text = sipradiInfos[section].type
        headerView.subTitleLbl.text = headerSubTitles[section]
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        tableView.register(FooterView.self, forHeaderFooterViewReuseIdentifier: footerViewId)
        
        let footerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: footerViewId) as! FooterView
        footerView.titleLbl.text = footerTitle[section]
        return footerView
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }

}
