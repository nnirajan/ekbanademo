import UIKit

class Network {
    static let main = Network()
    
    public func getData(completion: @escaping (Bool, Int?, Data?) -> Void) {
        guard let url = URL(string: getDataUrl) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                debugPrint("Network error: \(String(describing: error))")
                
                completion(false, nil, nil)
            } else {
                if let response = response as? HTTPURLResponse, let data = data {
                    debugPrint("Network response: \(String(describing: response))")
                    
                    if response.statusCode >= 200 && response.statusCode <= 299 {
                        debugPrint("Network data: \(String(bytes: data, encoding: .utf8) ?? "nil")")
                        
                        completion(true, response.statusCode, data)
                        return
                    } else {
                        completion(false, response.statusCode, nil)
                    }
                }
            }
        }.resume()
    }
    
    public func getImage(imageUrl: String, completion: @escaping (Bool, Int?, Data?) -> Void) {
        guard let imageUrl = URL(string: imageUrl) else { return }
        
        URLSession.shared.dataTask(with: imageUrl) { (data, response, error) in
            if let error = error {
                debugPrint("Network error: \(String(describing: error))")
                
                completion(false, nil, nil)
            } else {
                if let response = response as? HTTPURLResponse, let data = data {
                    debugPrint("Network response: \(String(describing: response))")
                    
                    if response.statusCode >= 200 && response.statusCode <= 299 {
                        debugPrint("Network data: \(String(bytes: data, encoding: .utf8) ?? "nil")")
                        
                        completion(true, response.statusCode, data)
                    } else {
                        completion(false, response.statusCode, nil)
                    }
                }
            }
        }.resume()
    }
    
}
