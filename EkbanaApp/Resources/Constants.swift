import UIKit

// ***** Networking Constants *****//

let baseUrl = "https://api.myjson.com"

var getDataUrl: String {
    get {
        return baseUrl + "/bins/z651w"
    }
}

// ***** UI constant ****** //

let tabelViewCellId = "CellId"
let headerID = "HeaderId"

// for banner
let bannerTableViewCellId = "BannerTVCId"
let bannerCollectionViewCellId = "BannerCVCID"
let bannerViewHeight: CGFloat = UIScreen.main.bounds.height * 0.35

// for quick links
let quickLinksTableViewCellId = "QuickLinksTVCId"
let quickLinksCollectionViewCellId = "QuickLinksCVCID"
let quickLinksViewHeight: CGFloat = 200

// for know more
let knowMoreViewHeight: CGFloat = UIScreen.main.bounds.width * 0.30

// for latest
let latestTableViewCellId = "LatestTVCId"
let latestCollectionViewCellId = "LatestCVCID"
let latestViewHeight: CGFloat = 250

// for trending
let trendingTableViewCellId = "TrendingTVCId"
let trendingCollectionViewCellId = "TrendingCVCID"
let trendingViewHeight: CGFloat = 250

// for batteries
let batteriesTableViewCellId = "BatteriesTVCId"
let batteriesCollectionViewCellId = "BatteriesCVCID"
let batteriesViewHeight: CGFloat = 300

// for lubricants
let lubricantsTableViewCellId = "LubricantsTVCId"
let lubricantsCollectionViewCellId = "LubricantsCVCID"
let lubricantsViewHeight: CGFloat = 190

// for nearby
let nearByTableViewCellId = "NearByTVCId"
let nearByViewHeight: CGFloat = 150

// for header footer view
let headerViewId = "HeaderViewId"
let footerViewId = "FooterViewId"

// textsize
let textSize1: CGFloat = 15
let textSize2: CGFloat = 14
let textSize3: CGFloat = 13
let textSize4: CGFloat = 12
let textSize5: CGFloat = 11
